# v0.1.1
Now detects where the header ends rather than assuming its a certain amount of bytes in,   
should make decrypting and encrypting more likely to work properly..  

Windows (x86): https://bitbucket.org/SilicaAndPina/registrydb0-tools/downloads/registrydb0-win32-0.1.1.zip  
Linux (x64): https://bitbucket.org/SilicaAndPina/registrydb0-tools/downloads/registrydb0-linux64-0.1.1.tar.gz  

# v0.1
how to use:
put registry.db0 into the same folder as this application
run the application
registry.db0-output is the processed file!

oh also! dont mess with the header, it will break everything if you do.. might fix that later..

Windows (x86): https://bitbucket.org/SilicaAndPina/registrydb0-tools/downloads/registrydb0-win32-0.1.zip  
Linux (x64): https://bitbucket.org/SilicaAndPina/registrydb0-tools/downloads/registrydb0-linux64-0.1.tar.gz  