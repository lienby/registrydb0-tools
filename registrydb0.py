def xor(data, key):
    l = len(key)
    return bytearray((
        (data[i] ^ key[i % l]) for i in range(0,len(data))
    ))

key = bytearray([0x89, 0xFA, 0x95, 0x48, 0xCB, 0x6D, 0x77, 0x9D, 0xA2, 0x25, 0x34, 0xFD, 0xA9, 0x35, 0x59, 0x6E])

reglines = open('registry.db0', 'r').readlines()
counter = 0
bytes = 0
while True:
    if reglines[counter].startswith(";#"):
        bytes += len(reglines[counter])
        counter += 1
    else:
        break

data = bytearray(open('registry.db0', 'rb').read())
registry = open('registry.db0-output', 'wb')
registry.write(str(data[:bytes]))
data = data[bytes:]
registry.write(str(xor(data, key)))
registry.close()

print "Done!"

